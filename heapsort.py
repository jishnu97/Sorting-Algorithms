import time
import heapq
import random
def heapsort(arr):
    sortedArr = [0]*len(arr)
    heapq.heapify(arr)
    for i in range(0,len(arr)):
        sortedArr[i]  = heapq.heappop(arr)
        heapq.heapify(arr)
    return sortedArr
arr=  random.sample(range(10000000000), 100000000)
#Monitor time
start_time = time.time()

sortArr = heapsort(arr)
if sorted(sortArr) == sortArr:
    print "Array is sorted"
    print "Time taken to run",
    print("--- %s seconds ---" % (time.time() - start_time))
