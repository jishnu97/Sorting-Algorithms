from time import sleep
from threading import Timer
import threading

import random

items = [3, 4, 5, 2, 1, 7]

def sleep_sort(i):
    sleep(i)
    print i

arr = [0]*len(items)
counter=0
for i in items:
    arr[counter] = (threading.Thread(target=sleep_sort, args=(i,)))
    arr[counter].start()
    counter+=1

for i in range(0,len(items)):
    arr[i].join()

